(function() {
    'use strict';
    
	angular.module('myApp').config(function($routeProvider) {
		$routeProvider
		.when("/", {
			templateUrl : "main.htm",
			controller : "mainController"
		})
		.when("/red", {
			templateUrl : "red.htm",
			controller : "redController"
		})
		
		.when("/blue", {
			templateUrl : "blue.htm",
			controller : "blueController"
		});
	});

	angular.module('myApp').controller('mainController', mainController);
	angular.module('myApp').controller('redController', redController);
	angular.module('myApp').controller('blueController', blueController);

    function mainController($scope) {
		$scope.msg = "Main";
    }
	
	function redController($scope) {
        $scope.msg = "Red";
    }
	
	function blueController($scope) {
        $scope.msg = "Blue";
    }
})();
